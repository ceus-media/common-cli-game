<?php
class CLI_GuessGame extends CLI_Application{

	protected function main(){
		$commands	= $this->arguments->get( 'commands' );
		if( isset( $commands[0] ) && $commands[0] === 'help' ){
			$this->showUsage();
			return;
		}
		$maxNumber	= intval( isset( $commands[0] ) ? $commands[0] : 3 );
		$maxTries	= intval( isset( $commands[1] ) ? $commands[1] : 3 );
		try{
			if( $maxNumber < 1 )
				throw new \RangeException( 'Argument "MAX" must be at least 1' );
			if( $maxNumber > 100 )
				throw new \RangeException( 'Argument "MAX" must be at most 100' );
			if( $maxTries < 1 )
				throw new \RangeException( 'Argument "TRIES" must be at least 1' );
			if( $maxTries > 10 )
				throw new \RangeException( 'Argument "TRIES" must be at most 10' );
			$this->run( $maxNumber, $maxTries );
		}
		catch( \RangeException $e ){
			CLI::error( 'Error: '.$e->getMessage().'.' );
		}
	}

	/**
	 *	Prints Usage Message to Console and exits Script, to be overridden.
	 *	@access		protected
	 *	@param		string		$message		Message to show below usage lines
	 *	@return		void
	 */
	protected function showUsage( $message = NULL ){
		CLI::out();
		CLI::out( 'Guess Game' );
		CLI::out();
		CLI::out( 'Usage: ./game.php [MAX] [TRIES]' );
		CLI::out( 'Options:' );
		CLI::out( '  MAX		Highest number (valid: 1-100, default: 3)' );
		CLI::out( '  TRIES		Amount of guesses (valid: 1-10, default: 3)' );
		if( $message )
			$this->showError( $message );
	}

	protected function run( $maxNumber, $maxTries ){
		$number		= rand( 1, $maxNumber );
		$success	= \FALSE;
		$question	= new \CLI_Question( 'Guess a number between 1 and '.$maxNumber.':', CLI_Question::TYPE_INTEGER );
		for( $i=0; $i<$maxTries; $i++){
			$guess	= $question->ask();
			if( ( $success = ( $guess == $number ) ) )
				break;
			$question	= new \CLI_Question( 'Nope. Again!', CLI_Question::TYPE_INTEGER );
		}
		if( $success ){
			$tries	= $i === 0 ? ( $i + 1 ).' try' : ( $i + 1 ).' tries';
			\CLI::out( 'YES! You have guessed the number in '.$tries );
		}
		else{
			\CLI::out( 'No. You lost the game.' );
		}
		\CLI::out( '' );
		if( \CLI_Question::askStatic( 'Again?', CLI_Question::TYPE_BOOLEAN, 'y' ) == "y" )
			$this->run( $maxNumber, $maxTries );
	}
}

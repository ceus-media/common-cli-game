#!/usr/bin/env php
<?php
(@include 'vendor/autoload.php') or die('Please use composer to install required packages.' . PHP_EOL);
require_once 'GuessGame.php';
new UI_DevOutput;

error_reporting( E_ALL );

try{
	$g	= new \CLI_GuessGame();
}
catch( \Exception $e ){
	CLI::error( 'Exception: '.$e->getMessage().'.' );
}
CLI::out();
